
##Keyframe and CSS Animation

Simple CSS Animation with a boat moving and a frog entering later

1. Define the keyframes in CSS
```
@keyframes KFname {
from or 0%	{property: 	value%;}
30% 				{property: 	value%;}
to or 100%	{property: 	value%;}
}
```
2. insert the keyframe into the selector defining name and duration
```
animation-name: KFname;
animation-duration: 000s;
animation-delay: 000s;
animation-timing-function: function (ex ease-out,linear)

[animation-direction:alternate,reverse,alternate-reverse]
[animation-fill-mode:backwards/forwards (first/last frame applies after iteration)]

[animation-iteration-count: int cycles or infinite;]
```

or shorthanded
```
animation: name duration delay timing-function;
```




